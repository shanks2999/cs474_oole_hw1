  lazy val root = (project in file("."))
  .settings(
	javacOptions ++= Seq("-encoding", "UTF-8"),
	libraryDependencies += "junit" % "junit" % "4.5",
	libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % Test
	)