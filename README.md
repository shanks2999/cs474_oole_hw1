# README #

# OOLE Assignment Homework 1 #


**Repository chosen:** Junidecode from GitHub

- **Link to Original Repo:** https://github.com/gcardone/junidecode

**Description:** It�s a library which is used to convert Unicode to 7-Bit ASCII. Helpful in removing diatrics from a string, convert Latin accents and other foreign language to their English representations.

**Junit Tests:** The application came with 2 of its own test cases, nevertheless I created my 2 custom test cases within `shanks.customTest.JUnitTest.java` file inside the `test<java` package.

**Main Method:** As this Junidecode is a library, main method was not created. So, I created a custom main method within `shanks.customExe.StartingClass.java` inside `main<java` package.

**Build Configurations:**

- **Gradle:** Created gradle script named `build.gradle` in project folder. Cleaned it using command `gradle clean` and built it using `gradle build`

	- **Reference:**  https://www.tutorialspoint.com/gradle/gradle_dependency_management.htm

	- **Snap:** ![Snapshot for Gradle build script](/snaps/gradle_build.jpg)
	
- **SBT:** Created sbt script named `build.sbt` in project folder. Compiled it using command `sbt compile` and rant it using `sbt test`

	- **Reference:**  http://www.scala-sbt.org/0.13/docs/Basic-Def.html

	- **Snap:** ![Snapshot for SBT build script](/snaps/sbt_build.jpg)

**Monitoring Tools:**

- **JConsole:** Ran JConsole from `java<bin` folder and connected the running process to get statistics.

	- **Snap:** 
	![Snapshot for JConsole Overview](/snaps/jconsole_overview.jpg)
	![Snapshot for JConsole memory](/snaps/jconsole_memory.jpg)
	![Snapshot for JConsole thread](/snaps/jconsole_threads.jpg)
	![Snapshot for JConsole VM summary](/snaps/jconsole_vm_summary.jpg)

- **JVisual VM:** Opened JVisualVM from `java<bin` folder and monitor Process of starting class

	- **Snap:** 
	![Snapshot for Java Visual VM](/snaps/java_visual_vm.jpg)

- **Visual GC:** From JVisualVm download VisualGC plugin and capture the monitoring stats

	- **Snap:** 
	![Snapshot for Java Visual GC](/snaps/java_visual_gc.jpg)

- **JStat:** From CMD call `jstat -gc <pid> <time interval in ms>`

	- **Snap:** 
	![Snapshot for JSTAT monitoring tool](/snaps/Jstat.jpg)
		
		
**BitBucket:**

- I forked the repository created and copied it to my system with command `git clone <URI>`
- Cloned project from github and pasted it in my forked repo and opened that project with IntelliJ.
- Created classes for main and tests.
- Built Gradle and SBT scripts.
- Compiled both and ran tests successfully.
- Captured snapshots from JConsole, VisualVM, VisualGC and JSTAT.
- Pasted all screenshots and Readme file into the folder and pushed into the BitBucket with command `git add -A`
- Committed code with command `git commit -m <�message�>`
- Pushed it using command `git push origin master`
- DONE !
	
**Limitation:**

- As my project is not a standalone application, I have to build seperate main class for it i.e. `StartingClass.java`
- My chosen project has execution time in micro seconds, therefore it was not feasible to capture snaps for the monitoring tools. In order to rectify that I had to put the code insite `while(true)` condition in order to record the stats.

**PS:** 

- If u want to view the snaps seperately, it is stored in the snaps folder in the repo.
- Ran all tests and compilation through CMD